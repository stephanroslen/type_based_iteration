// 2019 by Stephan Roslen

#include <iostream>
#include <string>
#include <type_traits>
#include <typeinfo>

namespace typelist {

template <typename ...Ts>
struct typelist;

template <typename T1, typename T2>
struct concat;

template <typename ...Ts1, typename ...Ts2>
struct concat<typelist<Ts1...>, typelist<Ts2...>> {
  using type = typelist<Ts1..., Ts2...>;
};

template <typename T1, typename T2>
using concat_t = typename concat<T1, T2>::type;

template <typename T>
struct headtail;

template <typename THead, typename ...TTail>
struct headtail<typelist<THead, TTail...>> {
  using head = THead;
  using tail = typelist<TTail...>;
};

template <typename T>
using head_t = typename headtail<T>::head;

template <typename T>
using tail_t = typename headtail<T>::tail;

template <typename Tlist, typename T>
struct successor;

template <typename Tlist, typename T>
using successor_t = typename successor<Tlist, T>::type;

template <typename T1, typename T2, typename ...Ts>
struct successor<typelist<T1, T2, Ts...>, T1> {
  using type = T2;
};

template <typename T, typename ...Ts>
struct successor<typelist<Ts...>, T> {
  using type = successor_t<tail_t<typelist<Ts...>>, T>;
};

}

template <typename Morphism, typename T>
struct application_result {
  using type = decltype(std::declval<Morphism>()(std::declval<T>()));
};

template <typename Morphism, typename T>
using application_result_t = typename application_result<Morphism, T>::type;

template <
    typename WhileCondTrait,
    typename Initial,
    typename Morphism,
    typename SFINAE = void>
struct generate_typecascade;

template <typename Initial, typename WhileCondTrait, typename Morphism>
using generate_typecascade_t =
    typename generate_typecascade<Initial, WhileCondTrait, Morphism>::type;

template <typename Initial, typename WhileCondTrait, typename Morphism>
struct generate_typecascade<
    Initial,
    WhileCondTrait,
    Morphism,
    std::enable_if_t<!WhileCondTrait::template value<Initial>>> {
  using type = typelist::typelist<Initial>;
};

template <typename Initial, typename WhileCondTrait, typename Morphism>
struct generate_typecascade<
    Initial,
    WhileCondTrait,
    Morphism,
    std::enable_if_t<WhileCondTrait::template value<Initial>>> {
  using type = typelist::concat_t<
      generate_typecascade_t<
          application_result_t<Morphism, Initial>,
          WhileCondTrait,
          Morphism>,
      typelist::typelist<Initial>>;
};

template <typename To, typename Initial, typename Cascade, typename Morphism>
To GetTypeIterationResult(Initial &initial, Morphism &morphism) {
  if constexpr (std::is_same_v<To, Initial>) {
    return initial;
  } else {
    using Next = typelist::successor_t<Cascade, To>;
    return morphism(
        GetTypeIterationResult<Next, Initial, Cascade, Morphism>(
            initial,
            morphism));
  }
}

template <typename Initial, typename WhileCondTrait, typename Morphism>
using result_t =
    typelist::head_t<generate_typecascade_t<Initial, WhileCondTrait, Morphism>>;

template <typename Initial_, typename WhileCondTrait, typename Morphism>
auto TypeBasedLoop(
    Initial_ &&initial,
    WhileCondTrait &&,
    Morphism &&morphism) -> result_t<
        std::remove_reference_t<Initial_>,
        WhileCondTrait,
        Morphism> {
  using Initial = std::decay_t<Initial_>;
  using Cascade = generate_typecascade_t<Initial, WhileCondTrait, Morphism>;
  using Result = typelist::head_t<Cascade>;

  return GetTypeIterationResult<Result, Initial, Cascade, Morphism>(
      initial,
      morphism);
}

struct CondTraitPtr {
  template <typename T>
  static constexpr bool value = std::is_pointer_v<std::remove_reference_t<T>>;
};

template <typename T>
decltype(auto) deref(T &ptr) {
  return TypeBasedLoop(
      ptr,
      CondTraitPtr{},
      [](const auto &input) -> auto & {
        return *input;
      });
}

template <unsigned int Iter_, unsigned int Sum_>
struct LoopSumState {
  static constexpr unsigned int Iter = Iter_;
  static constexpr unsigned int Sum = Sum_;
  std::string accu;
};

template <unsigned int N>
struct CondTraitLoopCountLess {
  template <typename T>
  static constexpr bool value = T::Iter < N;
};

template <typename N>
std::tuple <int, std::string> somealgo(const N &) {
  using LoopSumStateStart = LoopSumState<0, 0>;
  auto res = TypeBasedLoop(
      LoopSumStateStart{},
      CondTraitLoopCountLess<N::value>{},
      [](const auto &v) -> auto {
          using type = std::decay_t<decltype(v)>;
          return LoopSumState<type::Iter + 1, type::Sum + type::Iter>{
              v.accu + typeid(type).name()};
      });
  return {res.Sum, res.accu};
}

template <int I>
using integer_constant = std::integral_constant<int, I>;

int main(int, const char *[]) {
  int i = 5;
  auto ir = &i;
  auto irr = &ir;
  auto irrr = &irr;
  auto iv = deref(irrr);

  std::cout << "irrr completely dereferenced: " << iv << "\n";

  double d = 3.14;
  auto dr = &d;
  auto drr = &dr;
  auto drrr = &drr;
  auto drrrr = &drrr;
  auto drrrrr = &drrrr;
  auto dv = deref(drrrrr);

  std::cout << "drrrrr completely dereferenced: " << dv << "\n";

  auto [test00, test01] = somealgo(integer_constant<10>{});
  auto [test10, test11] = somealgo(integer_constant<20>{});

  std::cout << "test0: " << test00 << " " << test01 << "\n";
  std::cout << "test1: " << test10 << " " << test11 << "\n";

  return 0;
}

#if 0

decltype(auto) test(long ***********v) {
    return deref(v);
}

test(long***********):
  mov rax, QWORD PTR [rdi]
  mov rax, QWORD PTR [rax]
  mov rax, QWORD PTR [rax]
  mov rax, QWORD PTR [rax]
  mov rax, QWORD PTR [rax]
  mov rax, QWORD PTR [rax]
  mov rax, QWORD PTR [rax]
  mov rax, QWORD PTR [rax]
  mov rax, QWORD PTR [rax]
  mov rax, QWORD PTR [rax]
  ret

#endif
