// 2019 by Stephan Roslen

#include <iostream>
#include <type_traits>

namespace typelist {

template <typename ...Ts>
struct typelist;

template <typename T1, typename T2>
struct concat;

template <typename ...Ts1, typename ...Ts2>
struct concat<typelist<Ts1...>, typelist<Ts2...>> {
  using type = typelist<Ts1..., Ts2...>;
};

template <typename T1, typename T2>
using concat_t = typename concat<T1, T2>::type;

template <typename T>
struct headtail;

template <typename THead, typename ...TTail>
struct headtail<typelist<THead, TTail...>> {
  using head = THead;
  using tail = typelist<TTail...>;
};

template <typename T>
using head_t = typename headtail<T>::head;

template <typename T>
using tail_t = typename headtail<T>::tail;

template <typename Tlist, typename T>
struct successor;

template <typename Tlist, typename T>
using successor_t = typename successor<Tlist, T>::type;

template <typename T1, typename T2, typename ...Ts>
struct successor<typelist<T1, T2, Ts...>, T1> {
  using type = T2;
};

template <typename T, typename ...Ts>
struct successor<typelist<Ts...>, T> {
    using type = successor_t<tail_t<typelist<Ts...>>, T>;
};

}

template <typename Morphism, typename T>
struct application_result {
  using type = decltype(std::declval<Morphism>()(std::declval<T>()));
};

template <typename Morphism, typename T>
using application_result_t = typename application_result<Morphism, T>::type;

template <
    typename UntilCondTrait,
    typename Initial,
    typename Morphism,
    typename SFINAE = void>
struct generate_typecascade;

template <typename UntilCondTrait, typename Initial, typename Morphism>
using generate_typecascade_t =
    typename generate_typecascade<UntilCondTrait, Initial, Morphism>::type;

template <typename UntilCondTrait, typename Initial, typename Morphism>
struct generate_typecascade<
    UntilCondTrait,
    Initial,
    Morphism,
    std::enable_if_t<UntilCondTrait::template value<Initial>>> {
  using type = typelist::typelist<Initial>;
};

template <typename UntilCondTrait, typename Initial, typename Morphism>
struct generate_typecascade<
    UntilCondTrait,
    Initial,
    Morphism,
    std::enable_if_t<!UntilCondTrait::template value<Initial>>> {
  using type = typelist::concat_t<
      generate_typecascade_t<
          UntilCondTrait,
          application_result_t<Morphism, Initial>,
          Morphism>,
      typelist::typelist<Initial>>;
};

template <typename UntilCondTrait, typename Initial, typename Morphism>
struct TypeBasedLoop {
  template <typename UntilCondTrait_, typename Initial_, typename Morphism_>
  constexpr TypeBasedLoop(
      UntilCondTrait_ &&,
      Initial_ && initial,
      Morphism_ && morphism)
    : morphism_{std::forward<Morphism_>(morphism)},
      initial_{std::forward<Initial_>(initial)} {};
  using Cascade = generate_typecascade_t<UntilCondTrait, Initial, Morphism>;
  using Result = typelist::head_t<Cascade>;

  Morphism morphism_;
  Initial initial_;

  template <typename To>
  To GetTypeIterationResult(void) const {
    if constexpr (std::is_same_v<To, Initial>) {
      return initial_;
    } else {
      return morphism_(
          GetTypeIterationResult<typelist::successor_t<Cascade, To>>());
    }
  }

  Result operator()(void) const {
    return GetTypeIterationResult<Result>();
  }
};

template <typename UntilCondTrait_, typename Initial_, typename Morphism_>
TypeBasedLoop(UntilCondTrait_ &&, Initial_ && initial, Morphism_ && morphism)
    -> TypeBasedLoop<
        std::decay_t<UntilCondTrait_>,
        std::decay_t<Initial_>,
        std::decay_t<Morphism_>>;

struct CondTraitNonPtr {
  template <typename T>
  static constexpr bool value = !std::is_pointer_v<std::remove_reference_t<T>>;
};

constexpr auto DereferenceAsMuchAsPossible = [](const auto &p) -> auto & {
  return TypeBasedLoop(
      CondTraitNonPtr{},
      p,
      [](const auto &input)->auto & {
        return *input;
      })();
};

template <unsigned int N>
struct CondTraitLoopCountGreaterEqual {
  template <typename T>
  static constexpr bool value = T::Iter >= N;
};

template <unsigned int Iter_, unsigned int Sum_>
struct LoopSumState {
  static constexpr unsigned int Iter = Iter_;
  static constexpr unsigned int Sum = Sum_;
};

int main(int, const char *[]) {
  int i = 5;
  auto ir = &i;
  auto irr = &ir;
  auto irrr = &irr;
  auto iv = DereferenceAsMuchAsPossible(irrr);

  std::cout << "irrr completely dereferenced: " << iv << "\n";

  double d = 3.14;
  auto dr = &d;
  auto drr = &dr;
  auto drrr = &drr;
  auto drrrr = &drrr;
  auto drrrrr = &drrrr;
  auto dv = DereferenceAsMuchAsPossible(drrrrr);

  std::cout << "drrrrr completely dereferenced: " << dv << "\n";

  using LoopSumStateStart = LoopSumState<0, 0>;
  constexpr auto LoopSumCalc = [](const auto &v) -> auto {
    using type = std::decay_t<decltype(v)>;
    return LoopSumState<type::Iter + 1, type::Sum + type::Iter>{};
  };

  {
    auto il = TypeBasedLoop(
        CondTraitLoopCountGreaterEqual<10>{},
        LoopSumStateStart{},
        LoopSumCalc)();

    using iltype = decltype(il);
    std::cout
        << "Sum "
        << iltype::Sum
        << " after "
        << iltype::Iter
        << " iterations\n";
  }

  {
    auto il = TypeBasedLoop(
        CondTraitLoopCountGreaterEqual<20>{},
        LoopSumStateStart{},
        LoopSumCalc)();

    using iltype = decltype(il);
    std::cout
        << "Sum "
        << iltype::Sum
        << " after "
        << iltype::Iter
        << " iterations\n";
  }

  return 0;
}
