// 2019 by Stephan Roslen

#include <iostream>
#include <type_traits>

template <typename T>
decltype(auto) deref(T &ptr) {
  if constexpr(std::is_pointer_v<T>) {
    return deref(*ptr);
  } else {
    return ptr;
  }
}

int main(int, const char *[]) {
  int i = 23;
  auto ip = &i;
  auto ipp = &ip;
  auto ippp = &ipp;
  auto v = deref(ippp);
  std::cout << v << "\n";
  return 0;
}

#if 0

decltype(auto) test(long ***********v) {
    return deref(v);
}

test(long***********): # @test(long***********)
  mov rax, qword ptr [rdi]
  mov rax, qword ptr [rax]
  mov rax, qword ptr [rax]
  mov rax, qword ptr [rax]
  mov rax, qword ptr [rax]
  mov rax, qword ptr [rax]
  mov rax, qword ptr [rax]
  mov rax, qword ptr [rax]
  mov rax, qword ptr [rax]
  mov rax, qword ptr [rax]
  ret

decltype(auto) deref<long>(long&): # @decltype(auto) deref<long>(long&)
  push    rbp
  mov     rbp, rsp
  mov     qword ptr [rbp - 8], rdi
  mov     rax, qword ptr [rbp - 8]
  pop     rbp
  ret
decltype(auto) deref<long*>(long*&): # @decltype(auto) deref<long*>(long*&)
  push    rbp
  mov     rbp, rsp
  sub     rsp, 16
  mov     qword ptr [rbp - 8], rdi
  mov     rax, qword ptr [rbp - 8]
  mov     rdi, qword ptr [rax]
  call    decltype(auto) deref<long>(long&)
  add     rsp, 16
  pop     rbp
  ret
decltype(auto) deref<long**>(long**&): # @decltype(auto) deref<long**>(long**&)

#endif