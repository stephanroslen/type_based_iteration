// 2019 by Stephan Roslen

#include <iostream>
#include <type_traits>

template <typename T>
struct ptr_analyze {
  static constexpr size_t cnt = 0;
  using dereftype = T;
};

template <typename T>
struct ptr_analyze<T*> {
  static constexpr size_t cnt = ptr_analyze<T>::cnt + 1;
  using dereftype = typename ptr_analyze<T>::dereftype;
};

template <typename T>
constexpr size_t ptr_cnt_v = ptr_analyze<T>::cnt;

template <typename T>
using ptr_dereftype_t = typename ptr_analyze<T>::dereftype;

template <typename T>
decltype(auto) deref(T &ptr) {
  using ptr_t = std::remove_reference_t<T>;
  if constexpr (0 == ptr_cnt_v<ptr_t>) {
    return ptr;
  } else {
    using end_t = ptr_dereftype_t<ptr_t>;
    end_t **v = std::launder(reinterpret_cast<end_t **>(ptr));
    for (size_t i{0}; i < ptr_cnt_v<ptr_t> - 1; ++i) {
      v = std::launder(reinterpret_cast<end_t **>(*v));
    }
    return *std::launder(reinterpret_cast<end_t *>(v));
  }
}

int main(int, const char *[]) {
  int i = 23;
  auto ip = &i;
  auto ipp = &ip;
  auto ippp = &ipp;
  auto v = deref(ippp);
  std::cout << v << "\n";
  return 0;
}

#if 0

decltype(auto) test(long ***********v) {
    return deref(v);
}

test(long***********): # @test(long***********)
  mov     rax, rdi
  mov     ecx, 10
.LBB0_1: # =>This Inner Loop Header: Depth=1
  mov     rax, qword ptr [rax]
  dec     rcx
  jne     .LBB0_1
  ret

#endif